<?php

require_once __DIR__.'/../bootstrap.php';

use Symfony\Component\HttpFoundation\Request;

$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

$app->get('/', function() use($app) {
    return "Aggreg API - It's working";
});

$app->mount('/auth', new \Aggreg\Provider\AuthProvider($app));
$app->mount('/driver', new \Aggreg\Provider\DriverProvider($app));

$app->run();
 