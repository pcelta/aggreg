<?php
namespace Tests\Unit\Builder;

use Aggreg\Builder\DriverBuilder;

/**
 * @backupGlobals disabled
 */
class DriverBuilderTest extends \PHPUnit_Framework_TestCase
{
    public function testBuildShouldReturnDriverInstanceFullWhenValidPostData()
    {
        $postData = [
            "username"  => "pcelta",
            "password"  => "123456",
            "mail"      => "morilojunior@gmail.com",
            "name"      => "Pedro Ribeiro",
            "is_active" => "1"
        ];

        $result = DriverBuilder::build($postData);
        $this->assertInstanceOf("\\Aggreg\\Entity\\Driver", $result);
        $this->assertInstanceOf("\\Aggreg\\Entity\\User", $result->getUser());
        $this->assertInstanceOf("\\Aggreg\\Entity\\Person", $result->getPerson());

        $this->assertEquals("pcelta", $result->getUser()->getUsername());
        $this->assertEquals(32 , strlen($result->getUser()->getPassword()));

        $this->assertEquals("Pedro Ribeiro", $result->getPerson()->getName());
    }
} 