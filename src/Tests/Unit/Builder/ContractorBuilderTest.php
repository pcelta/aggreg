<?php
namespace Tests\Unit\Builder;

use Aggreg\Builder\ContractorBuilder;

/**
 * @backupGlobals disabled
 */
class ContractorBuilderTest extends \PHPUnit_Framework_TestCase
{
    public function testBuildShouldReturnContractorInstanceFullWhenValidPostData()
    {
        $postData = [
            "username"  => "mjunior",
            "password"  => "123456",
            "mail"      => "morilojunior@gmail.com",
            "name"      => "Morilo Junior",
            "is_active" => "1"
        ];

        $result = ContractorBuilder::build($postData);
        $this->assertInstanceOf("\\Aggreg\\Entity\\Contractor", $result);
        $this->assertInstanceOf("\\Aggreg\\Entity\\User", $result->getUser());
        $this->assertInstanceOf("\\Aggreg\\Entity\\Person", $result->getPerson());

    }
} 