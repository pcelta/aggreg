<?php


namespace Tests\Unit\Validate\Request\Driver;


use Aggreg\Validate\Request\Driver\CreateValidator;

class CreateValidatorTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @backupGlobals disabled
     */
    public function testIsValidShouldReturnTrueWhenValidData()
    {
        $data = [
            "mail"      => "aggreg@aggreg.com.br",
            "password"  => "1234567890",
            "is_active" => true,
            "name"      => "Agreguinho da Silva",
            "username"      => "aggregui",
            "documents"  => [["1"]]
        ];

        $mockedDocumentValidator = $this->getMockBuilder("Aggreg\\Validate\\Request\\Document\\CreateValidator")
            ->setMethods(["isValid"])
            ->getMock();

        $mockedDocumentValidator->expects($this->once())
            ->method('isValid')
            ->with(["1"])
            ->will($this->returnValue(true));

        $validator = new CreateValidator();
        $validator->setDocumentValidator($mockedDocumentValidator);

        $result = $validator->isValid($data);

        $this->assertTrue($result);
    }

    /**
     * @backupGlobals disabled
     */
    public function testIsValidShouldReturnFalseWhenInvalidDocuments()
    {
        $data = [
            "mail"      => "aggreg@aggreg.com.br",
            "password"  => "1234567890",
            "is_active" => true,
            "name"      => "Agreguinho da Silva",
            "username"      => "aggregui",
            "documents"  => [["1"]]
        ];

        $mockedDocumentValidator = $this->getMockBuilder("Aggreg\\Validate\\Request\\Document\\CreateValidator")
            ->setMethods(["isValid"])
            ->getMock();

        $mockedDocumentValidator->expects($this->once())
            ->method('isValid')
            ->with(["1"])
            ->will($this->returnValue(false));

        $validator = new CreateValidator();
        $validator->setDocumentValidator($mockedDocumentValidator);

        $result = $validator->isValid($data);

        $this->assertFalse($result);
    }

    /**
     * @backupGlobals disabled
     * @dataProvider providerMandatoriesFieldsMissing
     */
    public function testIsValidShouldReturnFalseWhenMandatoriesFieldsMissing($data)
    {
        $validator = new CreateValidator();
        $result = $validator->isValid($data);
        $this->assertFalse($result);
    }

    public static function providerMandatoriesFieldsMissing()
    {
        return [
            [
                [
                    "mail"      => "aggreg@aggreg.com.br",
                ]
            ],
            [
                [
                    "mail"      => "aggreg@aggreg.com.br",
                    "password"  => "1234567890",
                ]
            ],
            [
                [
                    "mail"      => "aggreg@aggreg.com.br",
                    "password"  => "1234567890",
                    "is_active" => true,
                ]
            ],
            [
                [
                    "mail"      => "aggreg@aggreg.com.br",
                    "password"  => "1234567890",
                    "is_active" => true,
                    "name"      => "Agreguinho da Silva",
                ]
            ],
            [
                [
                    "mail"      => "aggreg@aggreg.com.br",
                    "password"  => "1234567890",
                    "is_active" => true,
                    "name"      => "Agreguinho da Silva",
                    "username"      => "aggregui",
                ]
            ],
            [
                [
                    "password"  => "1234567890",
                    "is_active" => true,
                    "name"      => "Agreguinho da Silva",
                    "username"      => "aggregui",
                    "documents"  => []
                ]
            ],
            [
                [
                    "is_active" => true,
                    "name"      => "Agreguinho da Silva",
                    "username"      => "aggregui",
                    "documents"  => []
                ]
            ]
        ];
    }

    /**
     * @backupGlobals disabled
     * @dataProvider providerInvalidData
     */
    public function testIsValidShouldReturnFalseWhenInvalidData($data)
    {
        $mockedDocumentValidator = $this->getMockBuilder("Aggreg\\Validate\\Request\\Document\\CreateValidator")
            ->setMethods(["isValid"])
            ->getMock();

        $mockedDocumentValidator->expects($this->never())
            ->method('isValid');

        $validator = new CreateValidator();
        $validator->setDocumentValidator($mockedDocumentValidator);
        $result = $validator->isValid($data);
        $this->assertFalse($result);
    }

    public static function providerInvalidData()
    {
        return [
            [
                [
                    "mail"      => "aggregaggreg.com.br", // wrong
                    "password"  => "1234567890",
                    "is_active" => true,
                    "name"      => "Agreguinho da Silva",
                    "username"      => "aggregui",
                    "documents"  => []
                ]
            ],
            [
                [
                    "mail"      => "aggreg@aggreg.com.br",
                    "password"  => "1234567890",
                    "is_active" => 1, // wrong
                    "name"      => "Agreguinho da Silva",
                    "username"      => "aggregui",
                    "documents"  => []
                ]
            ],
            [
                [
                    "mail"      => "aggreg@aggreg.com.br",
                    "password"  => "1234567890",
                    "is_active" => true,
                    "name"      => "Agreguinho da Silva",
                    "username"      => "agg regui", //wrong
                    "documents"  => []
                ]
            ],
            [
                [
                    "mail"      => "aggreg@aggreg.com.br",
                    "password"  => "1234567890",
                    "is_active" => true,
                    "name"      => "",//wrong
                    "username"      => "aggregui",
                    "documents"  => []
                ]
            ],
            [
                [
                    "mail"      => "aggreg@aggreg.com.br",
                    "password"  => "12345", //wrong
                    "is_active" => true,
                    "name"      => "Agreguinho da Silva",
                    "username"      => "aggregui",
                    "documents"  => []
                ]
            ]
        ];
    }
} 