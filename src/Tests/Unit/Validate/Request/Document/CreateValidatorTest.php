<?php
namespace Tests\Unit\Validate\Request\Document;

use Aggreg\Validate\Request\Document\CreateValidator;

/**
 * @backupGlobals disabled
 */
class CreateValidatorTest extends \PHPUnit_Framework_TestCase
{

    public function testIsValidShouldReturnTrueWhenDataValid()
    {
        $data = [
            "number"        => "12343221",
            "expiration"    => "2014-01-01",
            "type"          => "cnh"
        ];

        $validator = new CreateValidator();
        $result = $validator->isValid($data);

        $this->assertTrue($result);
    }

    public function testIsValidShouldReturnFalseWhenExpirationDateInInvalidformat()
    {
        $data = [
            "number"        => "123432",
            "expiration"    => "2014-0101",
            "type"          => "cnh"
        ];

        $validator = new CreateValidator();
        $result = $validator->isValid($data);

        $this->assertFalse($result);
    }

    /**
     * @param $data
     * @dataProvider providerMandatoriesFieldsMissing
     */
    public function testIsValidShouldReturnFalseWhenMandatoriesFieldsMissing($data)
    {
        $validator = new CreateValidator();
        $result = $validator->isValid($data);

        $this->assertFalse($result);
    }

    public static function providerMandatoriesFieldsMissing()
    {
        return [
            [
                [
                    "number"        => "123432",
                    "expiration"    => "2014-01-01",
                ]
            ],
            [
                [
                    "expiration"    => "2014-0101",
                    "type"          => "cnh"
                ]
            ],
            [
                [
                    "number"        => "123432",
                    "expiration"    => "2014-0101"
                ]
            ],
            [
                [
                    "number"        => "123432",
                ]
            ],
            [
                [
                    "expiration"    => "2014-0101"
                ]
            ],
            [
                [
                    "type"    => "cnh"
                ]
            ],
            [[]]
        ];
    }

    /**
     * @param $data
     * @dataProvider providerInvalidData
     */
    public function testIsValidShouldReturnFalseWhenInvalidData($data)
    {
        $validator = new CreateValidator();
        $result = $validator->isValid($data);

        $this->assertFalse($result);
    }

    public static function providerInvalidData()
    {
        return [
            [
                [
                    "number"        => "123432",
                    "expiration"    => "2014-0101",
                    "type"          => "cnh"
                ]
            ],
            [
                [
                    "number"        => "123432",
                    "expiration"    => "2014-01-01",
                    "type"          => "c"
                ]
            ],
            [
                [
                    "number"        => "123432",
                    "expiration"    => "2014-01-01",
                    "type"          => "cc"
                ]
            ],
            [
                [
                    "number"        => "12345678",
                    "expiration"    => "2014-01-01",
                    "type"          => "r"
                ]
            ],
        ];
    }
} 