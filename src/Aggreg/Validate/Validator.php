<?php


namespace Aggreg\Validate;


interface Validator
{
    public function isValid(array $data);
} 