<?php
namespace Aggreg\Validate\Request\Driver;

use Aggreg\Validate\Validator;
use Aggreg\Validate\Request\Document\CreateValidator as DocumentValidator;
use Respect\Validation\Validator as v;

class CreateValidator implements Validator
{
    const USERNAME_KEY  = "username";
    const PASSWORD_KEY  = "password";
    const IS_ACTIVE_KEY = "is_active";
    const NAME_KEY      = "name";
    const MAIL_KEY      = "mail";
    const DOCUMENTS_KEY = "documents";

    /**
     * @var DocumentValidator
     */
    private $documentValidator;

    private static $mandatoriesFields = [
        self::USERNAME_KEY,
        self::PASSWORD_KEY,
        self::IS_ACTIVE_KEY,
        self::NAME_KEY,
        self::MAIL_KEY,
        self::DOCUMENTS_KEY
    ];

    /**
     * @return DocumentValidator
     */
    private function getDocumentValidator()
    {
        if (is_null($this->documentValidator)) {
            $this->documentValidator = new DocumentValidator();
        }

        return $this->documentValidator;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function isValid(array $data)
    {
        if (!is_array($data)) {
            return false;
        }

        foreach (self::$mandatoriesFields as $field) {
            if (!array_key_exists($field, $data)) {
                return false;
            }
        }

        $rules = v::arr()->key(self::USERNAME_KEY, v::alnum()->length(6, null)->noWhitespace())
                         ->key(self::PASSWORD_KEY, v::alnum()->length(6, null)->noWhitespace())
                         ->key(self::MAIL_KEY, v::email())
                         ->key(self::NAME_KEY, v::alpha()->notEmpty())
                         ->key(self::IS_ACTIVE_KEY, v::bool());

        if (!$rules->validate($data)) {
            return false;
        }

        foreach ($data["documents"] as $document) {
            if (!$this->getDocumentValidator()->isValid($document)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param DocumentValidator $documentValidator
     */
    public function setDocumentValidator(\Aggreg\Validate\Request\Document\CreateValidator $documentValidator)
    {
        $this->documentValidator = $documentValidator;
    }
} 