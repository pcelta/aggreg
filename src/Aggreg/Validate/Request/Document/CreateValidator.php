<?php
namespace Aggreg\Validate\Request\Document;

use Aggreg\Validate\Validator;
use Respect\Validation\Validator as v;

class CreateValidator implements Validator
{
    const EXPIRATION_KEY = "expiration";
    const NUMBER_KEY     = "number";
    const TYPE_KEY       = "type";

    private static $mandatoriesFields = [
        self::EXPIRATION_KEY, self::NUMBER_KEY, self::TYPE_KEY
    ];


    public function isValid(array $data)
    {
        if (!is_array($data)) {
            return false;
        }

        foreach (self::$mandatoriesFields as $field) {
            if (!array_key_exists($field, $data)) {
                return false;
            }
        }

        $rules = v::arr()->key(self::NUMBER_KEY, v::alnum()->noWhitespace()->length(8, 12))
                         ->key(self::EXPIRATION_KEY, v::date())
                         ->key(self::TYPE_KEY, v::alnum()->noWhitespace()->length(2, null));

        return $rules->validate($data);
    }
}