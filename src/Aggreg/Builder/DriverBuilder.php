<?php
namespace Aggreg\Builder;

use Aggreg\Entity\Driver;
use Aggreg\Entity\Person;
use Aggreg\Entity\User;

class DriverBuilder
{
    /**
     * @param array $postData
     * @return \Aggreg\Entity\Driver
     */
    public function build(array $postData)
    {
        $user = new User();
        $user->setIsActive(1);
        $user->setMail($postData['mail']);
        $user->setPassword($postData['password']);
        $user->setUsername($postData['username']);

        $person = new Person();
        $person->setName($postData['name']);

        $driver = new Driver();
        $driver->setIsActive(1);
        $driver->setUser($user);
        $driver->setPerson($person);

        return $driver;
    }
} 