<?php
namespace Aggreg\Builder;

use Aggreg\Entity\Contractor;
use Aggreg\Entity\User;
use Aggreg\Entity\Person;

class ContractorBuilder
{
    /**
     * @param array $postData
     * @return \Aggreg\Entity\Contractor
     */
    public function build(array $postData)
    {
        $user = new User();
        $user->setIsActive(1);
        $user->setMail($postData['mail']);
        $user->setPassword($postData['password']);
        $user->setUsername($postData['username']);

        $person = new Person();
        $person->setName($postData['name']);

        $driver = new Contractor();
        $driver->setIsActive(1);
        $driver->setUser($user);
        $driver->setPerson($person);

        return $driver;
    }
}