<?php


namespace Aggreg\Service;


use Aggreg\Builder\DriverBuilder;
use Aggreg\Validate\Request\Driver\CreateValidator;

class DriverService extends AbstractService
{
    private $driverBuilder;

    /**
     * @return DriverBuilder
     */
    private function getDriverBuilder()
    {
        if (is_null($this->driverBuilder)) {
            $this->driverBuilder = new DriverBuilder();
        }

        return $this->driverBuilder;
    }



    /**
     * @param array $postData
     * @return bool
     */
    public function create(array $postData)
    {
        $driver = $this->getDriverBuilder()->build($postData);
        $repository = $this->getRepository("DriverRepository");

        $repository->persist($driver);
    }
} 