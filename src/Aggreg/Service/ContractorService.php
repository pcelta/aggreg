<?php
namespace Aggreg\Service;

use Aggreg\Builder\ContractorBuilder;
use Aggreg\Validate\Request\Contractor\CreateValidator;

class ContractorService extends AbstractService
{
    private $contractorBuilder;

    /**
     * @return ContractorBuilder
     */
    private function getContractorBuilder()
    {
        if (is_null($this->contractorBuilder)) {
            $this->contractorBuilder = new ContractorBuilder();
        }

        return $this->contractorBuilder;
    }



    /**
     * @param array $postData
     * @return bool
     */
    public function create(array $postData)
    {
        $contractor = $this->getContractorBuilder()->build($postData);
        $repository = $this->getRepository("ContractorRepository");

        $repository->persist($contractor);
    }
} 