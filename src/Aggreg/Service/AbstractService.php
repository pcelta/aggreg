<?php


namespace Aggreg\Service;


use Silex\Application;

abstract class AbstractService
{
    /**
     * @var Application
     */
    protected $app;
    const BASE_REPOSITORY_NAMESPACE = "\\Aggreg\\Repository\\";

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    protected function getRepository($name)
    {
        $repository = self::BASE_REPOSITORY_NAMESPACE . $name;

        if (!class_exists($repository)) {
           throw new \InvalidArgumentException();
        }

        return new $repository($this->app["db.orm.em"]);
    }
} 