<?php


namespace Aggreg\Provider;

use Aggreg\Service\ContractorService;
use Aggreg\Validate\Request\Contractor\CreateValidator;
use Silex\Application;
use Silex\SilexEvents;
use Silex\Route;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class ContractorProvider implements  ControllerProviderInterface
{

    private $createValidator;

    /**
     * @return CreateValidator
     */
    private function getCreateValidator()
    {
        if (is_null($this->createValidator)) {
            $this->createValidator = new CreateValidator();
        }

        return $this->createValidator;
    }


    /**
     * @param Application $app An Application instance
     * @return ControllerCollection
     */
    public function connect(Application $app)
    {
        // creates a new controller based on the default route
        $controllers = $app['controllers_factory'];

        $controllers->post('/create', function (Application $app, Request $request) {

            $content = $request->getContent();
            $content = json_decode($content, true);

            if (is_null($content)) {
                return new JsonResponse(["error" => "invalid-json"]);
            }

            if (!$this->getCreateValidator()->isValid($content)) {
                return new JsonResponse(["error" => "invalid-data"]);
            }

            $driverService = new ContractorService($app);
            $driverService->create($content);
            return new JsonResponse(["success" => "true"]);
        });

        return $controllers;
    }
}