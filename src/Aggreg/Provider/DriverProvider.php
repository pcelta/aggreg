<?php


namespace Aggreg\Provider;

use Aggreg\Service\DriverService;
use Aggreg\Validate\Request\Driver\CreateValidator;
use Silex\Application;
use Silex\SilexEvents;
use Silex\Route;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class DriverProvider implements  ControllerProviderInterface
{

    private $createValidator;

    /**
     * @return CreateValidator
     */
    private function getCreateValidator()
    {
        if (is_null($this->createValidator)) {
            $this->createValidator = new CreateValidator();
        }

        return $this->createValidator;
    }


    /**
     * @param Application $app An Application instance
     * @return ControllerCollection
     */
    public function connect(Application $app)
    {
        // creates a new controller based on the default route
        $controllers = $app['controllers_factory'];

        $controllers->post('/create', function (Application $app, Request $request) {

            $content = $request->getContent();
            $content = json_decode($content, true);

            if (is_null($content)) {
                return new JsonResponse(["error" => "invalid-json"]);
            }

            if (!$this->getCreateValidator()->isValid($content)) {
                return new JsonResponse(["error" => "invalid-data"]);
            }

            $driverService = new DriverService($app);
            $driverService->create($content);
            return new JsonResponse(["success" => "true"]);
        });

        return $controllers;
    }
}