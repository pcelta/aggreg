<?php


namespace Aggreg\Provider;

use Silex\Application;
use Silex\SilexEvents;
use Silex\Route;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;

class AuthProvider implements ControllerProviderInterface
{

    /**
     * @param Application $app An Application instance
     * @return ControllerCollection
     */
    public function connect(Application $app)
    {
        // creates a new controller based on the default route
        $controllers = $app['controllers_factory'];

        $controllers->post('/signin', function (Application $app) {
            die("auth/singin");
        });

        return $controllers;
    }
} 