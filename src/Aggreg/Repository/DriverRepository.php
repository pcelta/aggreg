<?php


namespace Aggreg\Repository;


use Aggreg\Entity\Driver;

class DriverRepository extends AbstractRepository
{

    public function persist(Driver $driver)
    {
        try {

            parent::persist($driver);
        } catch (\Exception $e) {
            print_r($e->getMessage());
            die();
        }
    }

} 