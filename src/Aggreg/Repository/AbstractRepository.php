<?php


namespace Aggreg\Repository;

use \Doctrine\ORM\EntityManager;

abstract class AbstractRepository
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $entityManager
     */
    public function  __construct (EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    protected function persist($object)
    {
        $this->em->persist($object);
        $this->em->flush();
    }
}