<?php

$loader = require __DIR__.'/vendor/autoload.php';

$loader instanceOf \Composer\Autoload\ClassLoader;
$loader->add('Aggreg', __DIR__.'/src/');

use Aggreg\Manager\ParameterManager;

$app = new Silex\Application;

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => ParameterManager::getInstance()->get("db")
));

$app->register(
    new Nutwerk\Provider\DoctrineORMServiceProvider(),
    ParameterManager::getInstance()->get('doctrine.orm')
);